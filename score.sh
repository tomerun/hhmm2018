#!/bin/sh

echo "A"
grep point "$1" | head -n 100 | tail -n 100 | cut -f 2 -d = | ruby -e 'p STDIN.readlines.map(&:to_f).sum'
echo "B"
grep point "$1" | head -n 200 | tail -n 100 | cut -f 2 -d = | ruby -e 'p STDIN.readlines.map(&:to_f).sum'
echo "C"
grep point "$1" | head -n 300 | tail -n 100 | cut -f 2 -d = | ruby -e 'p STDIN.readlines.map(&:to_f).sum'
