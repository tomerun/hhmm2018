OBJ = main.o
TARGET = tester
COMPILEOPT = -Wall -Wno-sign-compare -O2 -std=gnu++1y -DLOCAL
vpath %.cpp ..
vpath %.h ..

.PHONY: all clean

all: $(TARGET)

$(TARGET): $(OBJ)
	g++-8 -o $@ $(OBJ)

%.o: %.cpp main.cpp
	g++-8 $(COMPILEOPT) -c $<

clean:
	rm -f $(TARGET)
	rm -f $(OBJ)
