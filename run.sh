#!/bin/sh

seed=${1:-1}
echo "seed = $seed"

for i in A B C; do
	for (( j = 100; j < 200; j++ )); do
		echo "$i $j"
		in_fn=`printf "input/input_%s_%3d.txt" "$i" "$j"`
		out_fn=`printf "output/result_%s_%3d.txt" "$i" "$j"`
		./tester < "$in_fn" > "$out_fn"
		scripts/output_checker "$in_fn" "$out_fn" "$seed"
	done
done

# for (( i = 100; i < 200; i++ )); do
# 	filename=input_A_`printf "%3d" "$i"`.txt
# 	scripts/generator input/"$filename" 1 "$i"
# done
# for (( i = 100; i < 200; i++ )); do
# 	filename=input_B_`printf "%3d" "$i"`.txt
# 	scripts/generator input/"$filename" 2 "$i"
# done
# for (( i = 100; i < 200; i++ )); do
# 	filename=input_C_`printf "%3d" "$i"`.txt
# 	scripts/generator input/"$filename" 3 "$i"
# done
