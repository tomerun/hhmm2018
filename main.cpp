#pragma GCC optimize ("O3")
#pragma GCC target ("avx")

#include <algorithm>
#include <utility>
#include <vector>
#include <bitset>
#include <string>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <set>
#include <iostream>
#include <array>
#include <tuple>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <sys/time.h>
#include "xmmintrin.h"
#include "emmintrin.h"
#include "pmmintrin.h"
#include "immintrin.h"

#ifdef LOCAL
// #define MEASURE_TIME
// #define DEBUG
#else
#define NDEBUG
// #define DEBUG
#endif
#include <cassert>

using namespace std;
using u8=uint8_t;
using u16=uint16_t;
using u32=uint32_t;
using u64=uint64_t;
using i64=int64_t;
using ll=int64_t;
using ull=uint64_t;
using vi=vector<int>;
using vvi=vector<vi>;

namespace {

#ifdef LOCAL
const double CLOCK_PER_SEC = 2.2e9;
const ll TL = 8500;
#else
const double CLOCK_PER_SEC = 2.5e9;
const ll TL = 28000;
#endif

// msec
ll start_time;

inline ll get_time() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	ll result =  tv.tv_sec * 1000LL + tv.tv_usec / 1000LL;
	return result;
}

inline ll get_elapsed_msec() {
	return get_time() - start_time;
}

inline bool reach_time_limit() {
	return get_elapsed_msec() >= TL;
}

inline ull get_tsc() {
#ifdef __i386
  ull ret;
  __asm__ volatile ("rdtsc" : "=A" (ret));
  return ret;
#else
  ull high,low;
  __asm__ volatile ("rdtsc" : "=a" (low), "=d" (high));
  return (high << 32) | low;
#endif
}

struct XorShift {
	uint32_t x,y,z,w;
	static const double TO_DOUBLE;

	XorShift() {
		x = 123456789;
		y = 362436069;
		z = 521288629;
		w = 88675123;
	}

	uint32_t nextUInt(uint32_t n) {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
		return w % n;
	}

	uint32_t nextUInt() {
		uint32_t t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8));
	}

	double nextDouble() {
		return nextUInt() * TO_DOUBLE;
	}
};
const double XorShift::TO_DOUBLE = 1.0 / (1LL << 32);

struct Counter {
	vector<ull> cnt;

	void add(int i) {
		if (i >= cnt.size()) {
			cnt.resize(i+1);
		}
		++cnt[i];
	}

	void print() {
		cerr << "counter:[";
		for (int i = 0; i < cnt.size(); ++i) {
			cerr << cnt[i] << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

struct Timer {
	vector<ull> at;
	vector<ull> sum;

	void start(int i) {
		if (i >= at.size()) {
			at.resize(i+1);
			sum.resize(i+1);
		}
		at[i] = get_tsc();
	}

	void stop(int i) {
		sum[i] += (get_tsc() - at[i]);
	}

	void print() {
		cerr << "timer:[";
		for (int i = 0; i < at.size(); ++i) {
			cerr << (int)(sum[i] / CLOCK_PER_SEC * 1000) << ", ";
			if (i % 10 == 9) cerr << endl;
		}
		cerr << "]" << endl;
	}
};

}

#ifdef MEASURE_TIME
#define START_TIMER(i) (timer.start(i))
#define STOP_TIMER(i) (timer.stop(i))
#define PRINT_TIMER() (timer.print())
#define ADD_COUNTER(i) (counter.add(i))
#define PRINT_COUNTER() (counter.print())
#else
#define START_TIMER(i)
#define STOP_TIMER(i)
#define PRINT_TIMER()
#define ADD_COUNTER(i)
#define PRINT_COUNTER()
#endif

#ifdef DEBUG
#define debug(format, ...) fprintf(stderr, format, __VA_ARGS__)
#define debugStr(str) fprintf(stderr, str)
#define debugln() fprintf(stderr, "\n")
#else
#define debug(format, ...)
#define debugStr(str)
#define debugln()
#endif

XorShift rnd;
Timer timer;
Counter counter;

template<class T>
inline T sq(T v) { return v * v; }

template<class T>
void shuffle(vector<T>& v) {
	if (v.size() <= 1) return;
	for (int i = 0; i < v.size() - 1; ++i) {
		int pos = rnd.nextUInt(v.size() - i) + i;
		swap(v[i], v[pos]);
	}
}

//////// end of template ////////

const double INITIAL_COOLER = 0.8 / 256.0;
const double FINAL_COOLER = 8.0 / 256.0;
const double INITIAL_PENA_RATIO = 4.0 * 256;
const double FINAL_PENA_RATIO = 13.0 * 256;
double sa_cooler;
double initial_pena_ratio = INITIAL_PENA_RATIO;
double pena_ratio;

int N, K;
vector<pair<int, int>> orig_con;

struct Term {
	int c;
	vi vars;
};

ostream& operator<<(ostream& s, const Term& t) {
	s << t.c;
	for (int v : t.vars) {
		s << " x_" << (v + 1);
	}
	return s;
}

struct Function {
	vector<Term> terms;
};

ostream& operator<<(ostream& s, const Function& f) {
	for (const Term& t : f.terms) {
		s << t << endl;
	}
	return s;
}

struct Result {
	double score;
	Function f;
	int max_coef;
};

struct Coefs {
	int c;
	vvi mat;

	void resize(int s) {
		mat.resize(s);
		for (vi& row : mat) {
			row.resize(s);
		}
	}

	void reassign(int s) {
		mat.resize(s);
		for (int i = 0; i < s; ++i) {
			mat[i].assign(s, 0);
		}
	}
};

struct Param {
	int max_coef;
	int reduc_th_max, reduc_th_min;
};

Function gather(const Function& f, int size) {
	Coefs coefs = {0};
	coefs.resize(size);
	for (const Term& t : f.terms) {
		if (t.vars.size() == 0) {
			coefs.c += t.c;
		} else if (t.vars.size() == 1) {
			coefs.mat[t.vars[0]][t.vars[0]] += t.c;
		} else {
			int v1 = min(t.vars[0], t.vars[1]);
			int v2 = max(t.vars[0], t.vars[1]);
			coefs.mat[v1][v2] += t.c;
		}
	}
	Function ret;
	if (coefs.c != 0) {
		ret.terms.push_back(Term({coefs.c, {}}));
	}
	for (int i = 0; i < coefs.mat.size(); ++i) {
		for (int j = i; j < coefs.mat.size(); ++j) {
			if (coefs.mat[i][j] == 0) continue;
			Term t;
			t.c = coefs.mat[i][j];
			t.vars.push_back(i);
			if (i != j) {
				t.vars.push_back(j);
			}
			ret.terms.push_back(t);
		}
	}
	return ret;
}

Function ntr_gbp(const Term& term, int& next_var) {
	assert(term.vars.size() == 3);
	assert(term.c < 0);
	int v1 = term.vars[0];
	int v2 = term.vars[1];
	int v3 = term.vars[2];
	Function f;
	f.terms.push_back(Term({term.c, {v1, next_var}}));
	f.terms.push_back(Term({-term.c, {v2, next_var}}));
	f.terms.push_back(Term({-term.c, {v3, next_var}}));
	f.terms.push_back(Term({term.c, {v1, v2}}));
	f.terms.push_back(Term({term.c, {v1, v3}}));
	f.terms.push_back(Term({-term.c, {v1}}));
	++next_var;
	return f;
}

Function ntr_kzfd(Term term, int& next_var, int max_coef) {
	assert(term.c < 0);
	int k = term.vars.size();
	if (k == 3 && abs(term.c) * 2 < max_coef) {
		return ntr_gbp(term, next_var);
	}
	shuffle(term.vars);
	int div = max_coef == 0 ? k : max(3, max_coef / abs(term.c) + 1);
	Function f;
	while (true) {
		if (k <= div) {
			Term additional = {-term.c * (k - 1), {next_var}};
			f.terms.push_back(additional);
			for (int v : term.vars) {
				Term quad = {term.c, {v, next_var}};
				f.terms.push_back(quad);
			}
			next_var++;
			break;
		}
		Term additional = {-term.c * (div - 1), {next_var}};
		f.terms.push_back(additional);
		for (int i = 0; i < div - 1; ++i) {
			int v = term.vars.back();
			term.vars.pop_back();
			Term quad = {term.c, {v, next_var}};
			f.terms.push_back(quad);
		}
		term.vars.push_back(next_var);
		k -= div - 2;
		next_var++;
	}
	return f;
}

Function ptr_gbp(const Term& term, int& next_var) {
	assert(term.c > 0);
	vi vs = term.vars;
	shuffle(vs);
	if (vs.size() > 8) {
		vs.resize(8);
	}
	int k = vs.size();

	Function f;
	for (int i = 0; i < k - 2; ++i) {
		int v1 = vs.back();
		vs.pop_back();
		int v2 = vs.back();
		vs.pop_back();
		f.terms.push_back(Term({term.c, {next_var}}));
		f.terms.push_back(Term({-term.c, {v1, next_var}}));
		f.terms.push_back(Term({-term.c, {v2, next_var}}));
		f.terms.push_back(Term({term.c, {v1, v2}}));
		vs.push_back(next_var);
		next_var++;
	}
	f.terms.push_back(Term({term.c, vs}));
	return f;
}

Function ptr_ishikawa(const Term& term, int& next_var) {
	assert(term.c > 0);
	int k = term.vars.size();
	Function f;
	for (int i = 0; i < k; ++i) {
		for (int j = i + 1; j < k; ++j) {
			f.terms.push_back(Term({term.c, {term.vars[i], term.vars[j]}}));
		}
	}
	int n = (k - 1) / 2;
	for (int i = 0; i < n; ++i) {
		int c = i == n && k % 2 == 1 ? 1 : 2;
		for (int j = 0; j < k; ++j) {
			f.terms.push_back(Term({-term.c * c, {term.vars[j], next_var}}));
		}
		f.terms.push_back(Term({term.c * (c * 2 * (i + 1) - 1), {next_var}}));
		next_var++;
	}
	return f;
}

void reduc_subst(vector<Term>& terms, int& next_var, const Param& param) {
	const int n = next_var;
	vvi pairs(next_var, vi(next_var));
	for (const Term& t : terms) {
		const int order = t.vars.size();
		for (int i = 0; i < order; ++i) {
			for (int j = i + 1; j < order; ++j) {
				int v1 = min(t.vars[i], t.vars[j]);
				int v2 = max(t.vars[i], t.vars[j]);
				pairs[v1][v2]++;
			}
		}
	}
	for (int i = 0; ; ++i) {
		int max_count = 1;
		vector<pair<int, int>> cands;
		for (int j = 0; j < n; ++j) {
			for (int k = j + 1; k < n; ++k) {
				if (pairs[j][k] > max_count) {
					max_count = pairs[j][k];
					cands = {{j, k}};
				} else if (pairs[j][k] == max_count) {
					cands.emplace_back(j, k);
				}
			}
		}
		int diff = param.reduc_th_max - param.reduc_th_min + 1;
		int th = (diff == 1 ? 0 : rnd.nextUInt(diff)) + param.reduc_th_min;
		if (max_count <= th) break;
		pair<int, int> sub = cands[rnd.nextUInt(cands.size())];
		int posi_sum = 0;
		int nega_sum = 0;
		for (Term& t : terms) {
			auto itr1 = find(t.vars.begin(), t.vars.end(), sub.first);
			auto itr2 = find(t.vars.begin(), t.vars.end(), sub.second);
			if (itr1 == t.vars.end() || itr2 == t.vars.end()) continue;
			if (t.c > 0) {
				if ((posi_sum + t.c) * 3 > param.max_coef) continue;
				posi_sum += t.c;
			} else {
				if ((nega_sum - t.c) * 3 > param.max_coef) continue;
				nega_sum -= t.c;
			}
			t.vars.erase(itr1);
			t.vars.erase(find(t.vars.begin(), t.vars.end(), sub.second));
			for (int j = 0; j < t.vars.size(); ++j) {
				int v1 = min(t.vars[j], sub.first);
				int v2 = max(t.vars[j], sub.first);
				if (v2 < n) {
					pairs[v1][v2]--;
				}
				v1 = min(t.vars[j], sub.second);
				v2 = max(t.vars[j], sub.second);
				if (v2 < n) {
					pairs[v1][v2]--;
				}
			}
			pairs[sub.first][sub.second]--;
			t.vars.push_back(next_var);
		}
		int coef = max(posi_sum, nega_sum);
		terms.push_back(Term({coef, {sub.first, sub.second}}));
		terms.push_back(Term({-2 * coef, {sub.first, next_var}}));
		terms.push_back(Term({-2 * coef, {sub.second, next_var}}));
		terms.push_back(Term({3 * coef, {next_var}}));
		next_var++;
	}
}

void split_common(vector<Term>& terms, int& next_var, const Param& param) {
	vector<Term*> ts;
	for (Term& t : terms) {
		if (t.vars.size() >= 2 && t.c > 0) {
			ts.push_back(&t);
		}
	}
	if (ts.size() < 2) return;
	int max_var = next_var;
	vector<pair<int, int>> used;
	vector<Term> created;
	int last_found = 0;
	for (int turn = 0; ; ++turn) {
		if (turn - last_found > 50) break;
		Term* t = ts[rnd.nextUInt(ts.size())];
		if (t->vars.size() < 2) continue;
		int p1 = rnd.nextUInt(t->vars.size());
		int p2 = rnd.nextUInt(t->vars.size() - 1);
		if (p2 >= p1) ++p2;
		int v1 = t->vars[p1];
		int v2 = t->vars[p2];
		if (v1 >= max_var || v2 >= max_var) continue;
		pair<int, int> vs = make_pair(v1, v2);
		if (find(used.begin(), used.end(), vs) != used.end()) continue;
		vector<Term*> ut = {t};
		int sum_c = t->c;
		for (Term* at : ts) {
			if (at == t) continue;
			if (sum_c + at->c > param.max_coef) continue;
			if (find(at->vars.begin(), at->vars.end(), v1) != at->vars.end()
				  && find(at->vars.begin(), at->vars.end(), v2) != at->vars.end()) {
				ut.push_back(at);
				sum_c += at->c;
			}
		}
		if (ut.size() <= 3) continue;
		created.push_back(Term({sum_c, {v1, v2, next_var}}));
		for (Term* utp : ut) {
			utp->vars.erase(find(utp->vars.begin(), utp->vars.end(), v1));
			utp->vars.erase(find(utp->vars.begin(), utp->vars.end(), v2));
			Term nt = {-utp->c, utp->vars};
			nt.vars.push_back(next_var);
			created.push_back(nt);
		}
		++next_var;
		last_found = turn;
	}
	terms.insert(terms.end(), created.begin(), created.end());
}

Function decrease_max_c(const Function& f, int max_c, int& next_var) {
	Function ret;
	int constant = 0;
	vi single_coef(next_var);
	for (const Term& t : f.terms) {
		if (t.vars.size() != 1) continue;
		single_coef[t.vars[0]] += t.c;
	}
	for (const Term& t : f.terms) {
		if (t.vars.empty()) {
			constant = t.c;
			continue;
		}
		if (t.vars.size() == 1) continue;
		int c = abs(t.c);
		if (c <= max_c) {
			ret.terms.push_back(t);
			continue;
		}
		int div = (c + max_c - 1) / max_c;
		int base = c / div;
		int rem = c % div;
		int nc = base;
		if (rem > 0) {
			nc++;
			rem--;
		}
		if (t.c > 0) {
			ret.terms.push_back(Term({nc, t.vars}));
			vi use_vars = t.vars;
			if (div == 2) {
				if (single_coef[t.vars[0]] < single_coef[t.vars[1]]) {
					swap(use_vars[0], use_vars[1]);
				}
			} else {
				use_vars.push_back(use_vars[0]);
			}
			for (int i = 0; i < div - 1; ++i) {
				nc = base;
				if (rem > 0) {
					nc++;
					rem--;
				}
				ret.terms.push_back(Term({nc, {use_vars[i], next_var}}));
				ret.terms.push_back(Term({-nc, {use_vars[i+1], next_var}}));
				single_coef[use_vars[i+1]] += nc;
				++next_var;
			}
		} else {
			ret.terms.push_back(Term({-nc, t.vars}));
			for (int i = 0; i < div - 1; ++i) {
				nc = base;
				if (rem > 0) {
					nc++;
					rem--;
				}
				ret.terms.push_back(Term({-nc, {t.vars[0], next_var}}));
				ret.terms.push_back(Term({-nc, {t.vars[1], next_var}}));
				ret.terms.push_back(Term({nc, {next_var}}));
				++next_var;
			}
		}
	}

	int start_var = next_var;
	int single_posi = 0;
	int single_nega = 0;
	for (int i = 0; i < single_coef.size(); ++i) {
		int c = abs(single_coef[i]);
		if (c <= max_c) {
			if (c != 0) ret.terms.push_back(Term({single_coef[i], {i}}));
			continue;
		}
		int div = (c + max_c - 1) / max_c;
		int base = c / div;
		int rem = c % div;
		if (single_coef[i] > 0) {
			single_posi += div - 1;
			for (int j = 0; j < div - 1; ++j) {
				int nc = base;
				if (rem > 0) {
					nc++;
					rem--;
				}
				ret.terms.push_back(Term({nc, {i, next_var}}));
				ret.terms.push_back(Term({-nc, {next_var}}));
				constant += nc;
				++next_var;
			}
			ret.terms.push_back(Term{base, {i}});
		} else {
			single_nega = max(single_nega, div - 1);
		}
	}
	vi nega_vars;
	for (int i = start_var; i < next_var; ++i) {
		nega_vars.push_back(i);
	}
	for (int i = 0; i < single_nega - single_posi; ++i) {
		nega_vars.push_back(next_var++);
	}
	for (int i = 0; i < single_coef.size(); ++i) {
		if (single_coef[i] >= -max_c) continue;
		int c = abs(single_coef[i]);
		int div = (c + max_c - 1) / max_c;
		int base = c / div;
		int rem = c % div;
		int nc = base;
		if (rem > 0) {
			nc++;
			rem--;
		}
		ret.terms.push_back(Term({-nc, {i}}));
		for (int j = 0; j < div - 1; ++j) {
			nc = base;
			if (rem > 0) {
				nc++;
				rem--;
			}
			ret.terms.push_back(Term({-nc, {i, nega_vars[j]}}));
		}
	}
	if (constant != 0) {
		ret.terms.push_back(Term({constant, {}}));
	}
	// cerr << f << endl << endl << ret << endl;
	return ret;
}

struct DecreaseEstimate {
	int new_var, new_term, actual_max_c;
};

DecreaseEstimate estimate_max_c(const Function& f, int next_var, int max_c) {
	DecreaseEstimate ret = {};
	bool has_const= false;
	vi single_coef(next_var);
	for (const Term& t : f.terms) {
		if (t.vars.size() != 1) continue;
		single_coef[t.vars[0]] += t.c;
	}
	for (const Term& t : f.terms) {
		if (t.vars.empty()) {
			has_const = true;
			continue;
		}
		if (t.vars.size() == 1) continue;
		int c = abs(t.c);
		if (c <= max_c) {
			ret.actual_max_c = max(ret.actual_max_c, c);
			continue;
		}
		int div = (c + max_c - 1) / max_c;
		ret.actual_max_c = max(ret.actual_max_c, (c + div - 1) / div);
		ret.new_var += div - 1;
		if (t.c > 0) {
			if (div >= 4) {
				// give up
				ret.new_var = 9999;
				ret.actual_max_c = 0;
				return ret;
			}
			ret.new_term += (div - 1) * 3;
			if (div == 2) {
				if (single_coef[t.vars[0]] < single_coef[t.vars[1]]) {
					single_coef[t.vars[0]] += t.c / div;
				} else {
					single_coef[t.vars[1]] += t.c / div;
				}
			} else {
				single_coef[t.vars[0]] += t.c / div;
				single_coef[t.vars[1]] += t.c / div;
			}
		} else {
			ret.new_term += (div - 1) * 3;
		}
	}

	int single_posi = 0;
	int single_nega = 0;
	for (int i = 0; i < single_coef.size(); ++i) {
		int c = abs(single_coef[i]);
		if (c <= max_c) {
			ret.actual_max_c = max(ret.actual_max_c, c);
			continue;
		}
		int div = (c + max_c - 1) / max_c;
		ret.actual_max_c = max(ret.actual_max_c, (c + div - 1) / div);
		if (single_coef[i] > 0) {
			single_posi += div - 1;
			ret.new_term += (div - 1) * 2;
		} else {
			single_nega = max(single_nega, div - 1);
			ret.new_term += div - 1;
		}
	}
	ret.new_var += single_posi;
	if (single_nega > single_posi) {
		ret.new_var += single_nega - single_posi;
	}
	if (!has_const && single_posi > 0) {
		ret.new_term++;
	}
	return ret;
}

Function postprocess(const Function& f, int& next_var) {
	Function tf = gather(f, next_var);
	shuffle(tf.terms);
	int max_c = 0;
	for (const Term& t : tf.terms) {
		if (t.vars.empty()) continue;
		max_c = max(max_c, abs(t.c));
	}
	double best_score = 1000.0 / ((next_var - N) * 5 + tf.terms.size() + 1000) * 1000.0 / (max_c + 1000);
	int best_max_c = max_c;
	for (int target_c = max_c - 1; target_c >= 5; ) {
		auto estimate = estimate_max_c(tf, next_var, target_c);
		double score = 1000.0 / ((next_var + estimate.new_var - N) * 5 + tf.terms.size() + estimate.new_term + 1000)
		               * 1000.0 / (estimate.actual_max_c + 1000);
		if (next_var + estimate.new_var >= 3000) score = 0;
		if (score > best_score) {
			best_score = score;
			best_max_c = target_c;
			// debug("var:%d term:%d actual_c:%d\n", next_var + estimate.new_var, (int)tf.terms.size() + estimate.new_term, estimate.actual_max_c);
		}
		if (score < best_score / 2) break;
		target_c = estimate.actual_max_c - 1;
	}
	debug("best_max_c:%d score:%.4f\n", best_max_c, best_score * 10000);
	Function decreased = decrease_max_c(tf, best_max_c, next_var);
	return gather(decreased, next_var);
}

Result solve_easy(vector<Term> input, Param param) {
	int next_var = N;
	shuffle(input);
	if (param.reduc_th_max > 0) {
		reduc_subst(input, next_var, param);
	}
	auto itr = partition(input.begin(), input.end(), [](const Term& t){ return t.vars.size() > 2; });
	vector<Term> obvious(itr, input.end());
	input.resize(itr - input.begin());
	Function f;
	for (const Term& t : input) {
		if (t.c > 0) {
			if (t.vars.size() == 4 && t.c * 3 <= param.max_coef) {
				Function partial = ptr_ishikawa(t, next_var);
				f.terms.insert(f.terms.end(), partial.terms.begin(), partial.terms.end());
			} else {
				Function partial = ptr_gbp(t, next_var);
				f.terms.insert(f.terms.end(), partial.terms.begin(), partial.terms.end());
			}
		} else {
			Function partial = ntr_kzfd(t, next_var, (int)(param.max_coef * (rnd.nextDouble() * 10 + 1)));
			f.terms.insert(f.terms.end(), partial.terms.begin(), partial.terms.end());
		}
	}
	f.terms.insert(f.terms.end(), obvious.begin(), obvious.end());
	Function ret = postprocess(f, next_var);
	double PY = 1000.0 / (1000 + 5 * (next_var - N) + ret.terms.size());
	int max_c = 0;
	for (const Term& t : ret.terms) {
		if (t.vars.empty()) continue;
		max_c = max(max_c, abs(t.c));
	}
	double PZ = 1000.0 / (1000 + max_c);
	double score = 10000.0 * PY * PZ;
	if (next_var >= 3000) score = 0;
	Result result = {score, ret};
	debug("var:%d term:%d max_c:%d\n", next_var, (int)ret.terms.size(), max_c);
	debug("max_coef:%d reduc_th:%d-%d PY:%.4f PZ:%.4f score:%.4f\n",
	      param.max_coef, param.reduc_th_min, param.reduc_th_max, PY, PZ, score);
	return result;
}

Result solve_easy_init(vector<Term>& input) {
	Param param = {0, 0, 0};
	Result best_result = {0.0};
	vector<double> scores = {best_result.score};
	for (int max_coef = 2000; max_coef >= 300; max_coef -= 10) {
		param.max_coef = max_coef;
		param.reduc_th_max = param.reduc_th_min = 0;
		Result res = solve_easy(input, param);
		if (res.score >= best_result.score) {
			best_result = res;
			best_result.max_coef = param.max_coef;
		}
		for (int reduc_th = 2; reduc_th <= 4; ++reduc_th) {
			param.reduc_th_max = param.reduc_th_min = reduc_th;
			if (get_elapsed_msec() > TL) break;
			Result res = solve_easy(input, param);
			scores.push_back(res.score);
			if (scores.size() >= 8) {
				scores.erase(scores.begin());
				double sum = 0;
				for (double s : scores) {
					sum += s;
				}
				if (sum * 2 < best_result.score * 8) {
					max_coef = 0;
					break;
				}
			}
			if (res.score >= best_result.score) {
				best_result = res;
				best_result.max_coef = param.max_coef;
			}
		}
		if (best_result.score < 2500) max_coef -= 20;
		if (best_result.score < 1500) max_coef -= 50;
	}
	if (best_result.max_coef <= 500 && best_result.score > 2500) {
		for (int max_coef = 290; max_coef >= 100; max_coef -= 5) {
			param.max_coef = max_coef;
			for (int reduc_th = 2; reduc_th <= 4; ++reduc_th) {
				param.reduc_th_max = param.reduc_th_min = reduc_th;
				if (get_elapsed_msec() > TL) break;
				Result res = solve_easy(input, param);
				scores.push_back(res.score);
				if (scores.size() >= 8) {
					scores.erase(scores.begin());
					double sum = 0;
					for (double s : scores) {
						sum += s;
					}
				if (sum * 2 < best_result.score * 8) {
						max_coef = 0;
						break;
					}
				}
				if (res.score >= best_result.score) {
					best_result = res;
					best_result.max_coef = param.max_coef;
				}
			}
		}
	}
	debug("best_score:%.4f\n", best_result.score);
	return best_result;
}

Result solve_easy(vector<Term>& input, int best_coef) {
	Param param = {0, 0, 0};
	Result best_result = {0.0};
	best_result.max_coef = best_coef;
	for (int turn = 0; ; ++turn) {
		if (get_elapsed_msec() > TL) {
			cerr << "solve_easy_turn:" << turn << endl;
			break;
		}
		param.max_coef = best_result.max_coef + rnd.nextUInt(201) - 100;
		if (param.max_coef < 50) param.max_coef = 50;
		if (rnd.nextUInt() & 1) {
			param.reduc_th_max = param.reduc_th_min = 0;
		} else {
			param.reduc_th_max = rnd.nextUInt(3) + 2;
			param.reduc_th_min = rnd.nextUInt(3) + 2;
		}
		if (param.reduc_th_max < param.reduc_th_min) {
			swap(param.reduc_th_min, param.reduc_th_max);
		}
		Result res = solve_easy(input, param);
		if (res.score > best_result.score) {
			best_result = res;
			best_result.max_coef = param.max_coef;
		}
	}
	debug("best_score:%.4f\n", best_result.score);
	return best_result;
}

Result solve_hard(vector<Term> input, double prob_nega_single) {
	shuffle(input);
	Coefs coefs = {0};
	coefs.resize(N);
	Result res = {0.0};
	vector<const Term*> posi_terms;
	for (const Term& t : input) {
		if (t.vars.size() == 0) {
			coefs.c += t.c;
		} else if (t.c < 0) {
			bool single = t.vars.size() <= 1 || rnd.nextDouble() < prob_nega_single;
			if (single) {
				for (int v : t.vars) {
					coefs.mat[v][v] += t.c;
				}
				coefs.c += -t.c * (t.vars.size() - 1);
			} else {
				int c = (abs(t.c) + t.vars.size() - 1) / t.vars.size();
				for (int i = 0; i < t.vars.size(); ++i) {
					for (int j = i; j < t.vars.size(); ++j) {
						coefs.mat[t.vars[i]][t.vars[j]] -= c;
					}
				}
				coefs.c += c * t.vars.size() * (t.vars.size() + 1) / 2 + t.c;
			}
		} else {
			posi_terms.push_back(&t);
		}
	}
	Coefs coefs_u = {};
	coefs_u.resize(N);
	for (int max_c = 300; max_c >= 0; max_c -= 5) {
		coefs_u.c = coefs.c;
		for (pair<int, int>& con : orig_con) {
			coefs_u.mat[con.first][con.second] = coefs.mat[con.first][con.second];
		}
		for (const Term* t : posi_terms) {
			int rest = t->c;
			int n = t->vars.size();
			for (int i = 0; i < n && rest > 0; ++i) {
				int v1 = t->vars[i];
				for (int j = i + 1; j < n; ++j) {
					int v2 = t->vars[j];
					if (coefs_u.mat[v1][v2] >= -max_c) continue;
					int diff = -max_c - coefs_u.mat[v1][v2];
					if (diff > rest) {
						coefs_u.mat[v1][v2] += rest;
						rest = 0;
						break;
					} else {
						coefs_u.mat[v1][v2] = -max_c;
						rest -= diff;
					}
				}
			}
			for (int i = 0; i < n && rest > 0; ++i) {
				int v1 = t->vars[i];
				if (coefs_u.mat[v1][v1] >= -max_c) continue;
				int diff = -max_c - coefs_u.mat[v1][v1];
				if (diff > rest) {
					coefs_u.mat[v1][v1] += rest;
					rest = 0;
					break;
				} else {
					coefs_u.mat[v1][v1] = -max_c;
					rest -= diff;
				}
			}
			for (int i = 0; i < n && rest > 0; ++i) {
				int v1 = t->vars[i];
				for (int j = i + 1; j < n; ++j) {
					int v2 = t->vars[j];
					if (coefs_u.mat[v1][v2] >= 0) continue;
					int diff = -coefs_u.mat[v1][v2];
					if (diff > rest) {
						coefs_u.mat[v1][v2] += rest;
						rest = 0;
						break;
					} else {
						coefs_u.mat[v1][v2] = 0;
						rest -= diff;
					}
				}
			}
			for (int i = 0; i < n && rest > 0; ++i) {
				int v1 = t->vars[i];
				if (coefs_u.mat[v1][v1] >= 0) continue;
				int diff = -coefs_u.mat[v1][v1];
				if (diff > rest) {
					coefs_u.mat[v1][v1] += rest;
					rest = 0;
					break;
				} else {
					coefs_u.mat[v1][v1] = 0;
					rest -= diff;
				}
			}
			if (rest > 0) {
				coefs_u.c += rest;
			}
		}
		int terms = coefs_u.c == 0 ? 0 : 1;
		int actual_max_c = 0;
		for (pair<int, int>& con : orig_con) {
			if (coefs_u.mat[con.first][con.second] != 0) {
				++terms;
				actual_max_c = max(actual_max_c, abs(coefs_u.mat[con.first][con.second]));
			}
		}
		double score = 5000.0 * (1000.0 / (terms + 1000)) * (1000.0 / (actual_max_c + 1000));
		// debug("%d %.4f\n", max_c, score);
		if (score > res.score) {
			res.score = score;
			res.f.terms.clear();
			res.f.terms.push_back(Term({coefs_u.c, {}}));
			for (pair<int, int>& con : orig_con) {
				if (coefs_u.mat[con.first][con.second] != 0) {
					if (con.first == con.second) {
						res.f.terms.push_back(Term({coefs_u.mat[con.first][con.second], {con.first}}));
					} else {
						res.f.terms.push_back(Term({coefs_u.mat[con.first][con.second], {con.first, con.second}}));
					}
				}
			}
		}
	}
	int next_var = N;
	Function ret = postprocess(res.f, next_var);
	double PY = 1000.0 / (1000 + 5 * (next_var - N) + ret.terms.size());
	int max_c = 0;
	for (const Term& t : ret.terms) {
		if (t.vars.empty()) continue;
		max_c = max(max_c, abs(t.c));
	}
	double PZ = 1000.0 / (1000 + max_c);
	double score = 5000.0 * PY * PZ;
	if (next_var >= 3000) score = 0;
	Result result = {score, ret};
	debug("var:%d term:%d max_c:%d PY:%.4f PZ:%.4f score:%.4f\n", next_var, (int)ret.terms.size(), max_c, PY, PZ, score);
	return result;
} 

Result solve_hard_init(vector<Term>& input) {
	Result best_result = {0.0};
	for (double prob_nega_single = 0.0; prob_nega_single <= 1.0; prob_nega_single += 0.02) {
		Result res = solve_hard(input, prob_nega_single);
		debug("prob:%.2f score:%.4f\n", prob_nega_single, res.score);
		if (res.score > best_result.score) {
			best_result = res;
		}
	}
	debug("best_score:%.4f\n", best_result.score);
	return best_result;
}

Result solve_hard(vector<Term>& input) {
	Result best_result = {0.0};
	for (int turn = 0; ; ++turn) {
		if (get_elapsed_msec() > TL) {
			cerr << "solve_hard_turn:" << turn << endl;
			break;
		}
		double prob_nega_single = 1.0 - sq(rnd.nextDouble());
		Result res = solve_hard(input, prob_nega_single);
		if (res.score > best_result.score) {
			best_result = res;
		}
	}
	return best_result;
}

Result solve_const(const vector<Term>& input, int orig_constant, int max_c, double best_score) {
	static vvi term_pos;
	if (term_pos.size() == 0) {
		term_pos.assign(N, vi(N));
	}
	vector<Term> terms;
	terms.push_back(Term({orig_constant, {}}));
	vi values(N);
	int last_fail = 0;
	int safe_turns = N < 14 ? (1 << N) : 10000;
	int rnd_turn = rnd.nextUInt() & 0xFFFF;
	for (int turn = 1; ; ++turn) {
		if (turn - last_fail == safe_turns) {
			break;
		}
		if (N < 14) {
			for (int i = 0; i < N; ++i) {
				values[i] = ((turn + rnd_turn) >> i) & 1;
			}
		} else {
			for (int i = 0; i < N; ++i) {
				values[i] = rnd.nextUInt() & 1;
			}
		}
		int fv = 0;
		for (const Term& t : input) {
			int tv = t.c;
			for (int v : t.vars) {
				if (values[v] == 0) {
					tv = 0;
					break;
				}
			}
			fv += tv;
		}
		int gv = 0;
		for (const Term& t : terms) {
			int tv = t.c;
			for (int v : t.vars) {
				if (values[v] == 0) {
					tv = 0;
					break;
				}
			}
			gv += tv;
		}
		if (gv >= fv) {
			continue;
		}
		last_fail = turn;
		int gap = fv - gv;
		vi zero_vars;
		vi one_vars;
		for (int i = 0; i < N; ++i) {
			if (values[i] == 0) {
				zero_vars.push_back(i);
			} else {
				one_vars.push_back(i);
			}
		}
		shuffle(zero_vars);
		shuffle(one_vars);
		for (int i = 0; i < zero_vars.size() && gap > 0; ++i) {
			int v1 = zero_vars[i];
			// f -> f + c(1 - v1) = f + c - c v1
			int& tp = term_pos[v1][v1];
			if (tp == 0) {
				tp = terms.size();
				terms.push_back(Term({0, {v1}}));
				if (5000.0 * 1000.0 / (terms.size() + 1000) * 1000.0 / (max_c + 1000) <= best_score) {
					goto GIVEUP;
				}
			}
			Term& t = terms[tp];
			int nc = min(gap, max_c + t.c);
			if (nc <= 0) continue;
			t.c -= nc;
			terms[0].c += nc;
			gap -= nc;
		}
		for (int i = 0; i < zero_vars.size() && gap > 0; ++i) {
			int v1 = zero_vars[i];
			for (int j = 0; j < one_vars.size() && gap > 0; ++j) {
				int v2 = one_vars[j];
				// f -> f + c(1 - v1)v2 = f + c v2 - c v1 v2
				int v_mi = min(v1, v2);
				int v_ma = v1 + v2 - v_mi;
				int& tp1 = term_pos[v2][v2];
				if (tp1 == 0) {
					tp1 = terms.size();
					terms.push_back(Term({0, {v2}}));
					if (5000.0 * 1000.0 / (terms.size() + 1000) * 1000.0 / (max_c + 1000) <= best_score) {
						goto GIVEUP;
					}
				}
				int& tp2 = term_pos[v_mi][v_ma];
				if (tp2 == 0) {
					tp2 = terms.size();
					terms.push_back(Term({0, {v_mi, v_ma}}));
					if (5000.0 * 1000.0 / (terms.size() + 1000) * 1000.0 / (max_c + 1000) <= best_score) {
						goto GIVEUP;
					}
				}
				Term& t1 = terms[tp1];
				Term& t2 = terms[tp2];
				int nc = min(gap, min(max_c - t1.c, max_c + t2.c));
				if (nc <= 0) continue;
				t1.c += nc;
				t2.c -= nc;
				gap -= nc;
			}
		}
		if (gap > 0) {
GIVEUP:
			// give up
			for (Term& t : terms) {
				if (t.vars.size() == 1) {
					term_pos[t.vars[0]][t.vars[0]] = 0;
				} else if (t.vars.size() == 2) {
					term_pos[t.vars[0]][t.vars[1]] = 0;
				}
			}
			Result res = {0.0};
			return res;
		}
	}

	int actual_max_c = 0;
	vector<Term> result_terms;
	int additional = 0;
	for (const Term& t : terms) {
		if (t.vars.size() == 1) {
			term_pos[t.vars[0]][t.vars[0]] = 0;
		} else if (t.vars.size() == 2) {
			term_pos[t.vars[0]][t.vars[1]] = 0;
		}
		if (t.c > 0) {
			additional += t.c;
		} else if (t.c < 0) {
			result_terms.push_back(t);
		}
		if (!t.vars.empty()) {
			actual_max_c = max(actual_max_c, abs(t.c));
		}
	}
	if (!result_terms.empty() && result_terms[0].vars.empty()) {
		result_terms[0].c += additional;
	} else {
		result_terms.push_back(Term({additional, {}}));
	}
	double PY = 1000.0 / (1000 + result_terms.size());
	double PZ = 1000.0 / (1000 + actual_max_c);
	double score = 5000.0 * PY * PZ;
	Result result = {score, Function({result_terms})};
	debug("term:%d max_c:%d PY:%.4f PZ:%.4f score:%.4f\n", (int)result_terms.size(), actual_max_c, PY, PZ, score);
	return result;
}

Result solve_const(vector<Term>& input) {
	int constant = 0;
	for (const Term& t : input) {
		constant += t.c;
	}
	debug("orig_const:%d\n", constant);

	Result best_result = {0.0};
	for (int max_c = 2; max_c <= 1000; max_c += 2) {
		if (5000 * 1000.0 / (1001) * 1000.0 / (max_c + 1000) <= best_result.score) break;
		if (get_elapsed_msec() > TL) break;
		Result res = solve_const(input, constant, max_c, best_result.score);
		if (res.score > best_result.score) {
			best_result = res;
		}
	}
	int max_max_c = (int)(5000.0 / best_result.score * (1000.0 / 1001.0) * 1000.0 - 1000.0);
	int min_max_c = max(1, max_max_c - 50);
	for (int turn = 1; max_max_c > 0; ++turn) {
		if (get_elapsed_msec() > TL) {
			cerr << "solve_const_turn:" << turn << endl;
			break;
		}
		int max_c = rnd.nextUInt(max_max_c - min_max_c + 1) + min_max_c;
		Result res = solve_const(input, constant, max_c, best_result.score);
		if (res.score > best_result.score) {
			best_result = res;
			max_max_c = (int)(5000.0 / best_result.score * (1000.0 / 1001.0) * 1000.0 - 1000.0);
			min_max_c = max(1, max_max_c - 50);
		}
	}
	debug("best_score:%.4f\n", best_result.score);
	return best_result;
}

Result solve_const_init(vector<Term>& input) {
	int constant = 0;
	for (const Term& t : input) {
		constant += t.c;
	}
	debug("orig_const:%d\n", constant);

	Result best_result = {0.0};
	for (int max_c = 2; max_c <= 1000; max_c += 2) {
		if (5000 * 1000.0 / (1001) * 1000.0 / (max_c + 1000) <= best_result.score) break;
		if (get_elapsed_msec() > TL) break;
		Result res = solve_const(input, constant, max_c, best_result.score);
		if (res.score > best_result.score) {
			best_result = res;
		}
	}
	debug("best_score:%.4f\n", best_result.score);
	return best_result;
}

Result solve(vector<Term>& input) {
	debugStr("solve_easy_init\n");
	Result res_easy = solve_easy_init(input);
	if (res_easy.score < 5000) {
		debugStr("solve_const_init\n");
		Result res_const = solve_const_init(input);
		if (res_const.score <= res_easy.score) {
			debugStr("solve_easy\n");
			Result res_all = solve_easy(input, res_easy.max_coef);
			return res_all.score > res_easy.score ? res_all : res_easy;
		} else {
			debugStr("solve_const\n");
			Result res_all = solve_const(input);
			return res_all.score > res_const.score ? res_all : res_const;
		}
	} else {
		debugStr("solve_easy\n");
		Result res_all = solve_easy(input, res_easy.max_coef);
		return res_all.score > res_easy.score ? res_all : res_easy;
	}
}

int main() {
	start_time = get_time();
	cin >> N >> K;
	vector<Term> terms(K);
	vector<vector<bool>> connection(N, vector<bool>(N));
	for (int i = 0; i < K; ++i) {
		int d;
		cin >> d >> terms[i].c;
		terms[i].vars.resize(d);
		for (int j = 0; j < d; ++j) {
			cin >> terms[i].vars[j];
			terms[i].vars[j]--;
			for (int k = 0; k < j; ++k) {
				connection[terms[i].vars[k]][terms[i].vars[j]] = true;
			}
			connection[terms[i].vars[j]][terms[i].vars[j]] = true;
		}
	}
	for (int i = 0; i < N; ++i) {
		for (int j = i; j < N; ++j) {
			if (connection[i][j]) {
				orig_con.emplace_back(i, j);
			}
		}
	}
	Result ans = solve(terms);
	debug("total best score:%.4f\n", ans.score);
	unordered_map<int, int> var_cvt;
	for (int i = 0; i < N; ++i) {
		var_cvt[i] = i + 1;
	}
	for (const Term& term : ans.f.terms) {
		for (int v : term.vars) {
			if (v >= N && var_cvt.count(v) == 0) {
				int s = var_cvt.size();
				var_cvt[v] = s + 1;
			}
		}
	}
	cout << var_cvt.size() << " " << ans.f.terms.size() << endl;
	for (const Term& term : ans.f.terms) {
		cout << term.vars.size() << " " << term.c;
		vi vars(term.vars);
		for (int& v : vars) {
			v = var_cvt[v];
		}
		sort(vars.begin(), vars.end());
		for (int v : vars) {
			cout << " " << v;
		}
		cout << endl;
	}
	cerr << "elapsed:" << get_elapsed_msec() << endl;
}
